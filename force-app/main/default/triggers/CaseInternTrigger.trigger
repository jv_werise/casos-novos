/*******************************************************************************
*                               Werise - 2021
*-------------------------------------------------------------------------------
*
*	Trigger on Case SObject for (Insert or Update or Delete or Undelete) 
*   && (After or Before)
*
* 	For Insert:
*   CaseInternTriggerHandler.adicionarContato(Trigger.new);
*	
*   
* NAME: CaseInternTrigger
* AUTHOR: João Vitor Ramos                                DATE: 28/01/2021
*******************************************************************************/
trigger CaseInternTrigger on Case (before insert, before update, before delete, after insert, after update, after undelete) {
    
    if(Trigger.isInsert){
        if(Trigger.isBefore){
            //calling the handler class to add the contact to case using the web email
            CaseInternTriggerHandler.adicionarContatoConta(Trigger.new);
        }
    }
    if(Trigger.isUpdate){
        if(Trigger.isBefore){
            //calling the handler class to close the CaseMilestone when changing the Case Status
            CaseInternTriggerHandler.fecharMarco(Trigger.new, Trigger.oldMap);
        }
    }
}